

--- README  -------------------------------------------------------------

Field FadeSlider

@todo - description here


--- INSTALLATION & USAGE-------------------------------------------------

@todo - update these
1 - Extract the archive into sites/{sitename}/modules directory
2 - Enable Field Slideshow at /admin/modules, (Image, Field, and Field UI required as well)
3 - Create or edit a content type at /admin/structure/types and include an Image field. Edit this image field to make it so that multiple image files may be added ("Number of values" setting at admin/structure/types/manage/{content type}/fields/{field_image}).
4 - Go to "Manage display" for your content type (/admin/structure/types/manage/{content type}/display) and switch the format of your multiple image field from Image to Slideshow.
5 - Click the settings wheel in the slideshow-formatted multple image field to edit advanced settings
6 - Save! and here you go.




--- CREDITS -------------------------------------------------------------

Much of this module's code is based on the code of the Field Slideshow module (http://drupal.org/project/field_slideshow).

FadeSlider jQuery plugin by Jason Lengstorf (http://copterlabs.com)
