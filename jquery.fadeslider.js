/**
 * Scripts that allow for AJAX editing
 *
 * LICENSE: Dual licensed under the MIT or GPL licenses.
 *
 * @author    Jason Lengstorf <jason.lengstorf@copterlabs.com>
 * @copyright 2011 Copter Labs, Inc.
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt  GPL License
 */
(function($){

    $.fn.FadeSlider = function( options )
    {
        var utils = $.fn.FadeSlider.utils,
            opts = $.extend(utils.defaults, options);

        return this.each(function(  ) {

            var debug    = $("#debug-info"),
                totalWidth = 0,
                slider   = $(this),
                slides   = slider.children()
                                .each(function(){
                                    var slide = $(this),
                                        width = slide.find("img").width();

                                    totalWidth += width;
                                    slide.width(width);
                                }),
                count    = slides.length,
                index    = 1,
                offset   = slider.offset(),
                styles = {
                    width: slider.width(),
                    height: slider.height(),
//                    left: Math.floor(offset.left),
                    marginTop: parseInt(slider.css("marginTop")),
                    marginRight: parseInt(slider.css("marginRight")),
                    marginBottom: parseInt(slider.css("marginBottom")),
                    marginLeft: parseInt(slider.css("marginLeft")),
                    borderTopWidth: parseInt(slider.css("borderTopWidth")),
                    borderRightWidth: parseInt(slider.css("borderRightWidth")),
                    borderBottomWidth: parseInt(slider.css("borderBottomWidth")),
                    borderLeftWidth: parseInt(slider.css("borderLeftWidth")),
                    borderTopColor: slider.css("borderTopColor"),
                    borderRightColor: slider.css("borderRightColor"),
                    borderBottomColor: slider.css("borderBottomColor"),
                    borderLeftColor: slider.css("borderLeftColor"),
                    borderTopStyle: slider.css("borderTopStyle"),
                    borderRightStyle: slider.css("borderRightStyle"),
                    borderBottomStyle: slider.css("borderBottomStyle"),
                    borderLeftStyle: slider.css("borderLeftStyle")
                },
                leftArrow = $("<a />",{
                        href: "#",
                        html: "&lsaquo;",
                        "class": "fs-left-arrow",
                        click: function( event ) {
                            event.preventDefault();

                            utils.slide();
                        }
                    }),
                rightArrow = $("<a />",{
                        href: "#",
                        html: "&rsaquo;",
                        "class": "fs-right-arrow",
                        click: function( event ) {
                            event.preventDefault();

                            utils.slide(-1);
                        }
                    }),
                caption = $("<p />",{
                        "class": "fs-caption"
                    }),
                ext_link = $("<p />",{
                        "class": "fs-ext-link"
                    }),
                wrapper  = $("<div />",{
                        "class" : "fs-wrapper",
                        css : styles
                    }),

                // Image counter
                counter = $("<p />",{
                        "class": "fs-counter",
                        html: "<span>" + index + "</span> | " + count
                    }),

                // Arrow images
                la_image = $("<img />",{
                        src: opts.leftArrow,
                        load: function() {
                            $(".fs-left-arrow")
                                .css({
                                    top: (styles.height-this.height)/2
                                });
                        }
                    }),
                ra_image = $("<img />",{
                        src: opts.rightArrow,
                        load: function() {
                            $(".fs-right-arrow")
                                .css({
                                    top: (styles.height-this.height)/2
                                });
                            $(".fs-counter")
                                .css({
                                    right: this.width*1.667,
                                    top: (styles.height - $('.fs-counter').height())/2
                                })
                        }
                    });

            // Debugging for slider dimensions
//            for( var x in styles )
//            {
//                utils.log(styles[x]+"px", x);
//            }

            slider
                .wrap(wrapper)
                .addClass("fs-slider")
                .css({
                    position: "absolute",
                    top: 0,
                    left: 0,
                    margin: 0,
                    border: 0,
                    width: totalWidth + count*opts.slideSpacing*2
                })
                .after(leftArrow, rightArrow, counter);

            // Adjust arrow top offsets
            $(".fs-left-arrow")
                .css({
                    backgroundColor: "transparent",
                    backgroundPosition: "0 0",
                    backgroundAttachment: "scroll",
                    margin: 0,
                    padding: 0
                })
                .html(la_image);
            $(".fs-right-arrow")
                .css({
                    backgroundColor: "transparent",
                    backgroundPosition: "0 0",
                    backgroundAttachment: "scroll",
                    top: (styles.height-ra_image.img_height)/2,
                    margin: 0,
                    padding: 0
                })
                .html(ra_image);

            slides
                .css({
                    float: "left",
                    padding: "0 "+opts.slideSpacing+"px"
                })
                .eq(0)
                    .addClass("current")
                    .each(function(){
                        var leftOffset = (styles.width-$(this).outerWidth())/2;

                        slider
                            .data("original-left", leftOffset)
                            .css("left", leftOffset);
                    })
                    .end()
                .not(".current")
                    .css("opacity", .5);

        });
    };

    $.fn.FadeSlider.utils = {
        defaults : {
                slideSpacing: 10,
                leftArrow: "/sites/forestwoodward.drufolio.com/modules/field_fadeslider/fs_left-arrow.png",
                rightArrow: "/sites/forestwoodward.drufolio.com/modules/field_fadeslider/fs_right-arrow.png"
            },
        slide : function( direction ) {
            var direction = direction || 1,
                slider = $(".fs-slider"),
                current = $(".current"),
                current_index = current.index(),
                current_width = current.outerWidth(),
                total_slides = $(".current").siblings().length + 1,
                next, next_width, new_left, move_distance;

            if( direction=="right" || direction==-1 )
            {
                direction = -1;
            }
            else
            {
                direction = 1;
            }

            if( current_index==0 && direction==1 )
            {
                return false;
            }

            if( current_index==total_slides-1 && direction==-1 )
            {
                next = slider.children().eq(0);
                new_left = slider.data("original-left");
            }
            else
            {
                if( direction==-1 )
                {
                    next = current.next();
                }
                else
                {
                    next = current.prev();
                }

                next_width = next.outerWidth();
                move_distance = ((current_width+next_width)/2)*direction;
                new_left = "+=" + move_distance+ "px";
            }

            next
                .addClass("current")
                .animate({
                    opacity: 1
                },
                {
                    duration: 800,
                    easing: "swing"
                })
                .siblings()
                    .removeClass("current")
                    .animate({
                        opacity: .5
                    },
                    {
                        duration: 800,
                        easing: "swing"
                    });

            slider
                .animate({
                    left: new_left
                },
                {
                    duration: 1000,
                    easing: "swing",
                    complete: function(  ) {
                        $(".fs-counter")
                            .find("span")
                                .text($(".current").index()+1);
                    }
                });
        },
        preload : function( image ) {
                return $("<img />",{
                        src : image
                    });
            },
        log : function( data, label ) {
                $("#debug-info").append(this.format_debug(data, label));
            },
        format_debug : function( data, label ) {
                label = label || "Debug";
                return $("<li>", {
                        html : "<strong>" + label + "</strong>: " + data
                    });
            }
    };

})(jQuery);
